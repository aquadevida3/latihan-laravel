@extends('layout.layout_adminlte')

@section('judul')
Page Cast  {{--INI BUAT JUDUL --}}
@endsection

@section('content')

<form action="/cast" method="POST">
    @csrf
    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" name="nama">
      </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" class="form-control" name="umur">
    </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label>Biodata</label>
        <input type="text" class="form-control" name="biodata">
      </div>
      @error('biodata')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    {{-- <div class="form-group form-check">
      <input type="checkbox" class="form-check-input" id="exampleCheck1">
      <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div> --}}
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection