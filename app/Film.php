<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    //klo di model ini menggunakan singular artinya di database tabelnya bentuk plural nya yaitu tabelnya bernama films
    //jadi klo kita tidak mau pakai bawaan laravel gunakan perintah protected $table = 'film';
    protected $table = 'film';
    protected $fillable =['judul','ringkasan','tahun','poster','cast_id'];

    public function cast(){
        return $this->belongsTo('App\Cast');//belongs to ke model bernama User
    }
    public function kritik(){
        return $this->hasMany('App\Kritik');
    }
    
}
