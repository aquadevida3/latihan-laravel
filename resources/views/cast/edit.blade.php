
@extends('layout.layout_adminlte')

@section('judul')
Edit Cast {{$cast->nama}}  {{--INI BUAT JUDUL --}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method("PUT")
    <div class="form-group">
        <label>Nama</label>
    <input type="text" value="{{$cast->nama}}" class="form-control" name="nama">
      </div>
        @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label>Umur</label>
      <input type="text" value="{{$cast->umur}}" class="form-control" name="umur">
    </div>
        @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label>Biodata</label>
        <input type="text" value="{{$cast->bio}}" class="form-control" name="biodata">
      </div>
      @error('biodata')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    {{-- <div class="form-group form-check">
      <input type="checkbox" class="form-check-input" id="exampleCheck1">
      <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div> --}}
    <button type="submit" class="btn btn-primary mr-2" >Update</button> <a href="/cast" class="btn btn-primary">Back</a>
</form>

@endsection




 