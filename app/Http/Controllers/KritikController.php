<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Kritik;

class KritikController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'input_kritik' => 'required',
            'rating' => 'required'
            
        ]);

        $kritik = new Kritik; 
        $kritik->film_id = $request->film_id;//film_id request dari view show.blade.php di folder film
        $kritik->user_id = Auth::id();//untuk id auth yang sedang login
        $kritik->input_kritik = $request->input_kritik;
        $kritik->rating = $request->rating;
        
        
        $kritik->save();

        return redirect()->back();
    }
}
