<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use illuminate\Support\Facades\Auth;
use App\Profile;

class ProfileController extends Controller
{   //function buat index
    public function index(){
        $profile = Profile::where('user_id', Auth::id())->first();
        return view('profile.index', compact('profile'));
    }
    //function buat update

    public function update(Request $request, $id){
        $request->validate([
            'umur' =>'required',
            'bio' =>'required',
            'alamat'=>'required'
        ]);
        
        $profile = Profile::find($id);

        $profile->umur = $request['umur'];
        $profile->bio = $request['bio'];
        $profile->alamat = $request['alamat'];
        
        $profile->save();

        return redirect ('/profil');
    }
    
}
