<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*default ROUTE
Route::get('/', function () {//maksudnya route : method('url/urinya apa', function())
    return view('welcome'); //tampilkan view -> welcome (ada di folder view->welcome.blade.php)
});

Route::get('/go', function (){
    return view('ngetes.gogo');
});

*/

Route::get('/', 'IndexController@index' );

Route::get('/regis', 'AuthController@regis');

Route::post('/sent', 'AuthController@sent');

//Route::get('/adminlte', 'AdminlteController@adminlte');

Route::get('/tables', 'TableController@tables');

Route::get('/datatables', 'TabledataController@datatables');

//ROUTE MIDDLEWARE
Route::group(['middleware'=>['auth']], function (){//route ini digunakan untuk mengautentifikasi seluruh route group dibawah ini (cast)
    //route CRUD tabel Cast di database datafilm==>

    Route::get('/cast','CastController@index');
    Route::get('/cast/create','CastController@create');
    Route::post('/cast','CastController@store');

    Route::get('/cast/{cast_id}','CastController@show'); 
    Route::get('/cast/{cast_id}/edit','CastController@edit');
    Route::put('/cast/{cast_id}','CastController@update');
    Route::delete('/cast/{cast_id}','CastController@destroy');

    //ROUTE UPDATE PROFILE
    
    Route::resource('profil','ProfileController')->only([
        'index','update'

    ]);

    //ROUTE KOMENTAR
    Route::resource('kritik','KritikController')->only([
        'index','store'

    ]);

});




//route CRUD tabel Game di database quiz3==>
Route::get('/game','GameController@index');
Route::get('/game/create','GameController@create');
Route::post('/game','GameController@game_store');

Route::get('/game/{game_id}','GameController@show'); 
Route::get('/game/{game_id}/edit','GameController@edit');
Route::put('/game/{game_id}','GameController@update');
Route::delete('/game/{game_id}','GameController@destroy');

//Route CRUD ORM
Route::resource('film','FilmController');//ga perlu lagi @ satu2 karna pakai resource dan buat controllernya tadi pakai php artisan make:controller FilmController --resource



Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home'); otomatis ketika instal npm dan npm run dev


