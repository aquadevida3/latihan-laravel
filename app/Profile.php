<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profil';
    protected $fillable =['umur','bio','alamat','user_id'];


    //ini untuk belongs to buat tabel yang ada foreign keynya
    public function user(){
        return $this->belongsTo('App\User');//belongs to ke model bernama User
    }//klo mau nampilin relasi one to one.. bisa lewat file index.blade.php atau di taro di side bar
}
