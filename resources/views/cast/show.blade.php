@extends('layout.layout_adminlte')

@section('judul')
Detail Cast {{$cast->nama}}    {{--INI BUAT JUDUL --}}
@endsection

@section('content')

<h1> {{$cast->nama}}</h1>
<p> {{$cast->umur}}</p><br>
<p> {{$cast->bio}}</p><br>

<div class="row">
    @foreach ($cast->film as $item)
        
    <div class="col-4">
        {{-- ini view dari one to many tabel film --}}
        <div class="card">
            <img height="200px" class="card-img-top" src="{{asset('poster/'.$item->poster)}}" alt="Card image cap">
            <div class="card-body">
             {{-- selipin disini one to many nya dari tabel cast --}}
           
            <h5>{{$item->judul}}</h5>
            <p class="card-text">{{$item->ringkasan}}</p>
            <p>Tanggal di Upload : {{($item->created_at)}}</p>
        {{-- form delete --}}
            </div>
        </div>
    </div>
    @endforeach

</div>




<a href="/cast" class="btn btn-primary mb-3">Back</a>

@endsection 