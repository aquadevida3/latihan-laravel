<?php //ini code GameController

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class GameController extends Controller
{
    public function index(){
        $game = DB::table('game')->paginate(4); //ini query untuk menampilkan seluruh isi tabel game seperti query sql select * from
        return view ('game.index', compact('game'));
    }
    public function create(){
        return view ('game.create');
    }

    public function game_store(Request $request ){
        //buat dulu validasi datanya dengan documentasi laravel supaya tidak sembarang input
        $request->validate([
            'name' => 'required',
            'game_play' => 'required',
            'developer'=> 'required',
            'year' => 'required',
            
        ]);

        DB::table('game')->insert([
            'name' => $request['name'],
            'game_play' => $request['game_play'],
            'developer' => $request['developer'],
            'year' => $request['year'] 
        ]);
        return redirect('/game');//untuk meredirect ketika fungsi tambah datanya dijalankan
    }
    public function show($id){
        $game = DB::table('game')->where('id', $id)->first();
        return view ('game.show', compact('game'));
    }

    public function edit($id){
        $game = DB::table('game')->where('id', $id)->first();
        return view ('game.edit', compact('game'));
    }

    public function update($id, Request $request){ //parameter id dan merequest nama dari label nama di create.blade.php
        $request->validate([
            'name' => 'required',
            'game_play' => 'required',
            'developer'=> 'required',
            'year' => 'required',
        ]);
        $affected = DB::table('game')//query update nya
              ->where('id', $id)
              ->update(
                [
                    'name' => $request['name'],
                    'game_play' => $request['game_play'],
                    'developer' => $request['developer'],
                    'year' => $request['year']
                ]
                );
                return redirect('/game');        
    }

    public function destroy($id){
        DB::table('game')->where('id', $id)->delete(); // where 'id', $id  sama aja kaya sql id = $id
        return redirect('/game');  
    }

}
