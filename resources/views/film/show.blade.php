@extends('layout.layout_adminlte')

@section('judul')
Detail Film {{$film->id}}    {{--INI BUAT JUDUL --}}
@endsection

@section('content')
<div class="row">
    <div class="col-4"> <!-- 12 dibagi 3 = 4-->
            <div class="card">
            <img height="200px" class="card-img-top" src="{{asset('poster/'.$film->poster)}}" alt="Card image cap">
            </div>
    </div>
</div>

<h1> {{$film->judul}}</h1>
<p> {{$film->ringkasan}}</p><br>
<p> {{$film->tahun}}</p><br>

<h1>Komentar</h1>
@foreach ($film->kritik as $item)
   <div class="card">
    <div class="card-body">
        <small><b>{{$item->user->name}} </b></small><p><small> user ini memberi nilai rating : {{$item->rating}}</small></p>
        <p class="card-text">{{$item->input_kritik}}</p>
    </div>
   </div>
@endforeach

<form action="/kritik" method="POST" enctype="multipart/form-data">
    @csrf
      
    <div class="form-group">
        {{-- <label>Content</label> --}}
        <input type="hidden" name="film_id" value="{{$film->id}}" id="">
        <textarea name="input_kritik" cols="50" rows="5"></textarea>
    </div>       
      @error('input_kritik')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Rating</label>
        <input type="number" name="rating" id="">
        {{-- <textarea name="input_kritik" cols="100" rows="10"></textarea> --}}
    </div>       
      @error('rating')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    

    <button type="submit" class="btn btn-primary">Save</button> <a href="/film" class="btn btn-primary">Back</a>

</form>





@endsection 