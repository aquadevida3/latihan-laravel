PENGERTIAN ORM (OBJECT RELATIONAL MAPPING)
=> MEMETAKAN ENTITAL DI DATABASE SEBAGAI OBJECT
=> ORM DI LARAVEL BERNAMA ELOQUENT (MODEL)

PERBEDAAN MENGGUNAKAN MODEL VS QUERY BUILDER

1. MODEL , ACTIVE RECORD PATTERN => SETIAP SATU BARIS DATA MEMILIKI FUNCTION SEPERTI INSERT, UPDATE DELETE DLL.

2. QUERY BUILDER , DAPAT MELAKUKAN LEBIH BANYAK CARA UNTUK QUERIES KE DATABASE

MEMBUAT MODEL (ELOQUENT) aturan penulisan Singular artinya tunggal ga pake spasi
$PHP ARTISAN MAKE:MODEL Item

maksudnya adalah model ini mendefinisikan tabel di database yang berkaitan dengan MOdel Item , jadi kalo mau buat CRUD di tabel film misal.. maka 
buat model nya $php artisan make:model Film jangan lupa huruf besar diawal penulisan Model.

//penulisan scrip pembuatan model juga bisa langsung buat migrationnya di laravel jika belum buat migration 
$php artisan make:model Film --migration

untuk penulisan buat model Film sekaligus buat function CRUD nya seperti INI: 
$ php artisan make:model Film --resource


jadi ketika kita buat model => Film Itu masuknya di folder app=>film.php
