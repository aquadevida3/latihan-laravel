<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kritik extends Model
{
    protected $table = 'kritik';
    protected $fillable =['film_id','user_id','input_kritik','rating'];
//belongs to ke tabel / model film dan user
    public function film(){
        return $this->belongsTo('App\Film');//belongs to ke model bernama User
    }
    public function user(){
        return $this->belongsTo('App\User');//belongs to ke model bernama User
    }

}
