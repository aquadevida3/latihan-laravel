<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast; //di tambah karna relasi orm 1 to many    

class CastController extends Controller
{
    public function index(){
        $cast = Cast::paginate(4);
        //$cast = Cast::all();
        // $cast = DB::table('cast')->paginate(4); //ini query untuk menampilkan seluruh isi tabel cast seperti query sql select * from
        return view ('cast.index', compact('cast'));
    }

    // public function pagination()
    // {
    //     $cast = DB::table('cast')->paginate(3);

    //     return view('cast.index', ['cast' => $cast]);
    // }

    public function create(){
        return view ('cast.create');
    }
    
    public function store(Request $request ){
        //buat dulu validasi datanya dengan documentasi laravel supaya tidak sembarang input
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'biodata'=> 'required',
        ]);

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['biodata'] 
        ]);
        return redirect('/cast');//untuk meredirect ketika fungsi tambah datanya dijalankan
    }

    public function show($id){
        //versi Eloquen (ORM)
        $cast = Cast::find($id);
        //versi Query Builder
        //$cast = DB::table('cast')->where('id', $id)->first();
        return view ('cast.show', compact('cast'));
    }
    public function edit($id){
        $cast = DB::table('cast')->where('id', $id)->first();
        return view ('cast.edit', compact('cast'));
    }

    public function update($id, Request $request){ //parameter id dan merequest nama dari label nama di create.blade.php
        $request->validate([
            'nama' => 'required',
            'umur' => 'required',
            'biodata'=> 'required',
        ]);
        $affected = DB::table('cast')//query update nya
              ->where('id', $id)
              ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['biodata']
                ]
                );
                return redirect('/cast');        
    }

    public function destroy($id){
        DB::table('cast')->where('id', $id)->delete(); // where 'id', $id  sama aja kaya sql id = $id
        return redirect('/cast');  
    }

}
