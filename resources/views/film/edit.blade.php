@extends('layout.layout_adminlte')

@section('judul')
EDIT FILM  {{$film->id}}{{--INI BUAT JUDUL --}}
@endsection

@section('content')

<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="form-group">
        <label>Judul</label>
        <input type="text" class="form-control" name="judul" value="{{$film->judul}}" placeholder="inputkan judul">
      </div>
        @error('judul')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
      <label>Ringkasan</label>
      <input type="text" class="form-control" value="{{$film->ringkasan}}" name="ringkasan" placeholder="isi ringkasan">
    </div>
        @error('ringkasan')
        <div class="alert alert-danger">{{ $message }}</div>
        @enderror
    <div class="form-group">
        <label>Tahun</label>
        <input type="text" value="{{$film->tahun}}" class="form-control" name="tahun">
      </div>
      @error('tahun')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
      <div class="form-group">
        <label>Cast</label>
        <select id="" class="form-control" name="cast_id">
            <option value="">-----pilih cast-----</option>
            @foreach ($cast as $item)
            @if ($item->id === $film->cast_id)
                <option value="{{$item->id}}"selected>{{$item->nama}}</option> 
            @else 
            <option value="{{$item->id}}">{{$item->nama}}</option> 
            @endif
            
            @endforeach
          </select>
      </div>
      @error('cast_id')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror

      <div class="form-group">
        <label>Poster</label>
        <input type="file" class="form-control" name="poster">
      </div>
      @error('poster')
      <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    
    <button type="submit" class="btn btn-primary">Update</button> <a href="/film" class="btn btn-primary">Back</a>

</form>

@endsection

