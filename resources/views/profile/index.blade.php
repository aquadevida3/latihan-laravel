@extends('layout.layout_adminlte')

@section('judul')
Halaman Update Profile  {{--INI BUAT JUDUL --}}
@endsection

@section('content')

<form action="/profil/{{$profile->id}}" method="POST">
@csrf
@method('PUT')

<div class="form-group">
    <label>Nama User</label>
    <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
    </div>
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control" value="{{$profile->user->email}}" disabled>
        </div>
    
    <div class="form-group"> 
    <label>Umur</label>
    <input type="number" class="form-control" name="umur" value="{{$profile->umur}}">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
    <label>Bio</label>
    <textarea name="bio" class="form-control">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

<div class="form-group">
<label>Alamat</label>
<textarea name="alamat" class="form-control">{{$profile->alamat}}</textarea>
</div>
@error('alamat')
<div class="alert alert-danger">{{ $message }}</div>
@enderror





<button type="submit" class="btn btn-primary">Submit</button> <a href="/profil" class="btn btn-primary">Back</a>

</form>

@endsection